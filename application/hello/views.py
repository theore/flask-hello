# -*- coding: utf-8 -*-
import inspect
import logging

from flask import render_template
from flask import request, jsonify, Blueprint


bp_hello = Blueprint('hello', __name__, url_prefix='/hello')


@bp_hello.route('/', methods=['GET'])
def home():
    return render_template('index.html')
